/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Seb
 */
public class PartieTest {
    
    
    @Test
    public void testPartieEstTermineeQuandChampsPris() {
        Partie p = new Partie();
        p.nbChampsPris = 9;
        assert(p.partieEstTerminee());
    }
    
    @Test
    public void testPartieEstTermineeQuandChampsLibres() {
        Partie p = new Partie();
        assert(!p.partieEstTerminee());
        
        p.nbChampsPris = 8;
        assert(p.partieEstTerminee());
        
        p.nbChampsPris = 10;
        assert(p.partieEstTerminee());
    }
    
}
